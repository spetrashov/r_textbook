---
title: "Введение в язык программирования R"
author: 'Ph. A. Upravitelev'
date: "2022-02-04"
output: word_document
documentclass: book
always_allow_html: true
bibliography:
- bibliography.bib
- packages.bib
# biblio-style: apalike
link-citations: yes
lang: ru-RU
site: bookdown::bookdown_site
description: "Short introduction into R programming language"
---

# Intro {-}



## Об учебнике {-}

С R я познакомился летом 2012 года. Начинавшие тогда же или ранее помнят, насколько мало было материалов или каких-то учебников. С учебниками по R и сейчас не особо радостно, так как первоначальная ориентированность на анализ данных и статистику сыграла с R злую шутку: практически все учебники стараются дать минимально необходимую базовую информацию о языке и побыстрее перейти к темам по статистике. В результате получается нечто промежуточное: концепции и идеи R как языка программирования описываются в таких учебниках очень слабо, а для того, чтобы быть хорошим учебником по статистике, не хватает глубины.

Учебник посвящен R именно как языку программирования, в отличие от общепринятого представления, что R --- среда для статистического анализа. Я старался делать больше акцент на то, как организован язык, чем на его использование для тех или иных задач анализа данных. Продвинутые пользователи увидят в тексте переклички с "Advanced R" Хэдли Викхэма, более опытные --- отсылки к документации языка (местами даже к коду), текстам Томаса Майлунда, различным блогам и статьям разработчиков.

Первые десять глав будут полезны и доступны всем, кто только начинает изучать язык. Последующие главы больше раскрывают логику и конструкцию языка ([*Выполнение выражений*](#eval) и [*ООП*](#oop)), а также некоторые темы, полезные при разработке. Подобная неоднородность порождает несколько нелогичные на первый взгляд ситуации, когда, например, базовые типы рассматриваются сразу в нескольких главах. Тем не менее, это сознательное решение, чтобы учесть разный опыт возможных читателей.

Я старался не выходить за пределы `base R`, поэтому многие темы, такие как синтаксис `data.table`/`tibble`, визуализации в `ggplot2`/`plotly`, асинхронное программирования, API-серверы или Shiny-приложения остались за пределами учебника. Многие из этих тем рассматриваются или будут рассматриваться в отдельном проекте [*R packages*](packages.rintro.ru).

## Благодарности {-}

Учебник вырос из задачек и конспектов, которые я готовил для студентов своих курсов по анализу данных. Группа МАР181 --- ван лав, мне было интересно писать для вас эти материалы.

Весь проект не состоялся бы без Артема Клевцова, моего друга и наставника на пути изучения R. Когда-то мы оба пришли к необходимости перейти с SPSS на R, с тех пор были и совместные проекты, и постепенная специализация каждого в свои области, но постоянный обмен мнениями и опытом сохраняются.

Отдельная благодарность рецензентам, которые читали и вычитывали текст: Андрею Огурцову, Григорию Демину и Дмитрию Богданову. Ваши замечания помогли мне быть аккуратнее в формулировках и внимательнее к деталям. А без помощи Анны Басниной запятых, опечаток и странных выражений в тексте было бы намного больше.

## Лицензия {-}

Это произведение доступно по лицензии [*CC BY-NC-ND 4.0*](http://creativecommons.org/licenses/by-nc-nd/4.0/).

## R intro {-}

Учебник является частью проекта [*R Intro*](rintro.ru), который также включает в себя конспекты по конкретным пакетам, сборники задач, записи вебинаров и прочие материалы. Обновление материалов проекта ведётся по принципу пакетов, то есть с некоторой эпизодичностью и журналом изменений.

## Контакты {-}

Процесс правок может быть бесконечен. Если у вас есть какие-то замечания, предложения или комментарии по тексту учебника, оставьте их, пожалуйста, в [*Issues*](https://gitlab.com/upravitelev/r_intro/-/issues). С прочими вопросами пишите мне (\@konhis, Philipp Upravitelev) в [*telegram*](t.me/konhis) или в [*ODS*](https://app.slack.com/client/T040HKJE3/D1D8W8QG4).

## Поддержать {-}

Если вам понравились материалы или идея проекта [*R Intro*](rintro.ru) и его разделов, вы можете купить мне [**чашечку кофе**](https://yoomoney.ru/to/410018512568186) или порадовать ежемесячной [**подпиской**](https://boosty.to/rintro). Для тех, кто не в России, так же есть [**ko-fi**](https://ko-fi.com/rintro).


